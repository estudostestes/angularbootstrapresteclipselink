package estudos.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

//Thread.currentThread().getContextClassLoader().getResourceAsStream
public class Util {
//	Constantes
	public static final String EXTENSAO_PROPERTIES = ".properties";
	public static final String VALIDATION_MESSAGE_PT_BR = "ValidationMessage-pt-br.properties";
	public static final String VALIDATION_MESSAGE_EN = "ValidationMessage-EN.properties";
	public static final String ERRO_CARREGAR_ARQUIVO = "Erro ao tentar carregar o arquivo: %s";
	
	public static String cortarDaStringAte(String texto, String corte) throws RegraException{
		if(isNotVazio(texto) && isNotVazio(corte) && texto.length() > corte.length()){
			texto = texto.substring(0, texto.indexOf(corte));
		}else{
			lancarError("Error ao cortar da estring!");
		}
		return texto;
	}
	
	
	public static boolean isNotVazio(String texto){
		if(texto != null && texto != ""){
			return true;
		}else{
			return false;
		}
	}
	
	public static void lancarError(String mensagem) throws RegraException{
		throw new RegraException(mensagem);
	}
	
	public static String getValueProperties(String chave, String nomeArquivo) throws RegraException {
		String msg = "";
		try {
			Properties arquivo = new Properties();
			if(!nomeArquivo.endsWith(EXTENSAO_PROPERTIES)){
				nomeArquivo += EXTENSAO_PROPERTIES;
			}
			InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream("/" + nomeArquivo);
			arquivo.load(in);
			msg = arquivo.getProperty(chave);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RegraException(String.format(ERRO_CARREGAR_ARQUIVO, nomeArquivo));
		}
		return msg;
	}
}	
