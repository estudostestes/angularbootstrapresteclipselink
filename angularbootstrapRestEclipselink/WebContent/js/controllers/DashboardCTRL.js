appPrincipal.controller("DashboardCTRL",  function($scope, utilHttp, vUrlsRest, vPath, utilDialog, vMsg, utilFunc, $timeout){
//	utilDialog.preLoad($scope, null, 1000, "sm");
	$scope.titulo = "";
	$scope.estilo = "form-danger";
	$scope.teste1 = "Nome do Usuário";
	$scope.largura = "3";
	$scope.msgRetorno = "Erro!";
	$scope.usuario = {};
	$scope.usuario.data = "15/04/2016";
	$scope.usuario.setor = "almoxerifado";
	$scope.usuario.curso = "Java Programmer";
	$scope.usuario.filial = "LO";
	$scope.notificacoes = "369";
	$scope.notificacoesUsuario = 1;
	$scope.msgPopover = "Realizando testes para visualização do popover no HTML - Realizando testes para visualização do popover no HTML - Realizando testes para visualização do popover no HTML";
	$scope.loading = false;
	$scope.loadingCicle = false;
	$scope.msgLoadings = "MSG Global!";
	$scope.isSelected = false;
	$scope.usuario.radioTeste = "FE";
	$scope.filiais = [{id:1,nome:"Pernambuco"}, {id:2,nome:"Alogoas"}, {id:3,nome:"Rio de Janeiro"}, {id:4,nome:"São Paulo"},
	                  {id:5,nome:"Ceará"},{id:6,nome:"Acre"}, {id:7,nome:"Minas Gerais"}];
	$scope.selecionados = [];//[{id:5,nome:"Ceará"},{id:6,nome:"Acre"}];
	$scope.selecionado = {};//{id:3,nome:"Rio de Janeiro"};
	$scope.listaCursos = {};
	$scope.filiaisSelecionados = [{id:3,nome:"Rio de Janeiro"}, {id:4,nome:"São Paulo"}];
	$scope.cursosSelecionados = [{id: 33, nome:"Nova InFo"}];
	$scope.check1 = {id: 33, nome: "Informátcia"};
	$scope.check2 = {id: 34, nome: "Inglês Britânico"};
	
	$scope.usuarioEdit = {};
	$scope.telaCadUsuario;
	
	$scope.testeDialog = function(){
		utilDialog.showMsg("<h3>Teste</h3> Mais tese", "Aviso!", "", "modal-sm");
	}
	$scope.testeModal = function(){
		$scope.telaCadUsuario = utilDialog.showModal($scope, null, vMsg.CADASTRO_USUARIO, null, null, null , null, false, vPath.PAGINAS + "/usuario/cadastro-usuario.html");
	}
	
	$scope.testeFunction = function(){
		utilDialog.showSlideRight($scope, "Iniciando" , 30);
	}
	$scope.testeFunctionPage = function(){
		utilDialog.showSlideRight($scope, "Iniciando" , 40, vPath.PAGINAS + "/usuario/cadastro-usuario.html");
	}
	
	$scope.clickRadio = function(value){
		utilDialog.showMsg("O valor selecionado é: " + value, "Teste de Radio", "atencao", "modal-sm");
	}
	
	$scope.setSelecionado = function(){
		$scope.selecionado = {id:3,nome:"Rio de Janeiro"};
	}
	$scope.setSelecionados = function(){
		$scope.selecionados = [{id:6,nome:"Acre"}, {id:7,nome:"Minas Gerais"}];
	}
	
	$scope.clicou = function(){
		utilDialog.showMsg("Testando o UI-BUTTON!");
	}
	
	$scope.mostrarLoading = function(){
		$scope.loading = !$scope.loading;
	}
	
	$scope.mostrarLoadingCicle = function(){
		$scope.loadingCicle = !$scope.loadingCicle;
	}
	
	$scope.openLoading = function(){
		utilDialog.openLoading($scope);
	}
	
	$scope.usuarioPesq = {};
	$scope.usuarios = [];
	$scope.retornoTabelaUsuario = {};
	
	
	$scope.limparFiltrosUsuario = function(){
		$scope.usuarioPesq = {};
	};
	
	$scope.openLoading = function(){
		utilDialog.openLoading($scope, null, "sm");
	}
	
	$scope.items = [{id:1,name:"Java"},{id:2,name:"Python"},{id:3,name:"JQuery"},{id:4,name:"AngularJs"}];
        $scope.selected = [];
        
        $scope.listaAbas = [{label: "Aba offline 1", page: vPath.PAGINAS + "/usuario/cadastro-usuario.html"}, {label: "Aba offline 2"}, {label: "Aba offline 3"}, {label: "Aba offline 4"}]
        
	$scope.addTab = function(){
		$scope.listaAbas.push({label: "Nova TAB"});
	}

});