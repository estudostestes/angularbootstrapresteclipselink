var appPrincipal = angular.module("angularbootstrapRestEclipselink",
["ngAnimate","utilService", "utilValue", "uiDirective", "ngRoute"]); 

appPrincipal.config(function($routeProvider, $locationProvider, $httpProvider) {
    
//	$httpProvider.interceptors.push('tokenInterceptor');
    
    $locationProvider.hashPrefix('');
    $locationProvider.html5Mode(true);
    
    $routeProvider.when('/dashboard', {
        templateUrl: '/estudos/paginas/tabs.html',
        controller: 'DashboardCTRL'
    });
  
//    $routeProvider.when('estudos/tabs', {
//        templateUrl: 'estudos/paginas/tabs.html',
//        controller: 'DashboardCTRL'
//    });
  
//    $routeProvider.otherwise({redirectTo: '/paginas/tabs.html'});

});